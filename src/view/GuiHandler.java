package view;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import controller.Webclient;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 *
 * @author leo
 */
public class GuiHandler
{
	// instance variabele
	private Label label;
	private TextField urlField;
	private TextArea textArea;
	private Button btnSend;
	private Button btnClear;

	
	
	// constructor
	public GuiHandler(GridPane pane)
	{
		// default graphics settings  
        pane.setVgap(5);
        pane.setHgap(8);
        pane.setPadding(new Insets(10,15,20,15));
		
		label = new Label("HTTP or HTTPS Get-request (include http:// or https://");
		urlField = new TextField();
		textArea = new TextArea();
		textArea.setMinHeight(200);
		textArea.setMinWidth(500);

		btnSend = new Button("Send");
		btnClear = new Button("Clear");
		
		btnSend.setOnAction(event -> getURL(urlField.getText()));
		btnClear.setOnAction(event -> clear());
		
		pane.add(label, 0, 0, 2, 1);
		pane.add(urlField, 0, 1, 2, 1);
		pane.add(btnSend, 0, 2);
		pane.add(btnClear, 1, 2);
		pane.add(textArea, 0, 3, 2, 1);
		
	}

	public Label getLabel()
	{
		return label;
	}

	public TextArea getTextArea()
	{
		return textArea;
	}
	
	private void getURL(String urlStr)
	{
		Webclient webclient = new Webclient();
		StringBuilder content = null;
		
		try
		{
			content = webclient.getURL(urlStr);
		} 
		catch (MalformedURLException exmalformed)
		{
			textArea.clear();
			textArea.setStyle("-fx-text-fill: red ;");
			textArea.appendText("URL is niet in het juiste formaat\n");
			textArea.appendText(exmalformed.toString());
			//textArea.setStyle("-fx-text-fill: black ;");
		}
		catch (UnknownHostException exhost)
		{
			textArea.clear();
			textArea.setStyle("-fx-text-fill: red;");
			textArea.appendText("Hostname bestaat niet\n");
			textArea.appendText(exhost.toString());
			//textArea.setStyle("-fx-text-fill: black ;");
		}
		catch (IOException exio)
		{
			textArea.clear();
			textArea.setStyle("-fx-text-fill: red ;");
			textArea.appendText("Bufferreader error\n");
			textArea.appendText(exio.toString());
			//textArea.setStyle("-fx-text-fill: black ;");
		}
		finally
		{
			if (content != null)
			{
				textArea.clear();
				textArea.setStyle("-fx-text-fill: black ;");
				textArea.setText(content.toString());
			}
		}
	}
	
	private void clear()
	{
		urlField.requestFocus();
		urlField.selectAll();	
		textArea.clear();
	}
}
