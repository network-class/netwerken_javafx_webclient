package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class Webclient
{

	public StringBuilder getURL(String urlStr) throws MalformedURLException, UnknownHostException, IOException
	{

		StringBuilder content = new StringBuilder();
		URL url = new URL(urlStr);

		content.append("Protocol: " + url.getProtocol() + "\n");// Using getProtocol() method of the URL class
		content.append("Host Name: " + url.getHost() + "\n"); // Using getHost() method
		content.append("Port Number: " + url.getPort() + "\n"); // Using getPort() method
		content.append("File Name: " + url.getFile() + "\n"); // Using getFile() method
		content.append("===================================\n");
		
		// throw exceptions to Gui
		
		URLConnection urlConnection = url.openConnection(); // creating a urlconnection object

		// wrapping the urlconnection in a bufferedreader
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String line;
		// reading from the urlconnection using the bufferedreader
		while ((line = bufferedReader.readLine()) != null)
		{
			content.append(line + "\n");
		}
		bufferedReader.close();
		
		return(content);
	}

}